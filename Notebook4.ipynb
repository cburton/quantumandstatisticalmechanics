{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Solving the Time-dependent Schr&ouml;dinger Equation\n",
    "## Background\n",
    "Here, we will solve the time dependent Schr&ouml;dinger equation, $$i\\hbar \\partial_t \\psi(x,t)=-\\frac{\\hbar^2}{2m} \\partial_x^2 \\psi(x,t)+V(x) \\psi(x,t).$$\n",
    "\n",
    "The first step is discretizing space, allowing us to replace the functions $\\psi(x)$ with vectors $\\vec\\psi$: $$i \\partial_t\\vec \\psi(t)=H\\vec\\psi(t),$$ using units where $\\hbar=1$.\n",
    "\n",
    "The next step is to make a finite difference in time. We will use the Semi-Implicit Method: $$\\quad i\\frac{\\vec\\psi(t+\\delta t)-\\vec \\psi(t)}{\\delta t}= H\\frac{\\vec\\psi(t+\\delta t)+\\vec\\psi(t)}{2},$$ which can be rewritten as $$\\vec\\psi(t+\\delta t)= \\left(1+\\frac{i H \\delta t}{2}\\right)^{-1} \\left(1-\\frac{i H \\delta t}{2}\\right)\\vec\\psi(t)$$\n",
    "This is a matrix equation, and we will use Gaussian Elimination to solve it at each timestep."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import TDSE\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell allows you to create tkinter GUI windows from a jupyter notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%gui tk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gaussian_wavepacket(x, mu, sigma, k):\n",
    "    '''Creates a gaussian wavepacket with wave-vector k.'''\n",
    "    initial = -0.5 * ((x - mu) / sigma)**2\n",
    "    evolution = 1j * k * x\n",
    "    return np.exp(initial + evolution)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Potential Functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def hardwall_potential(x):\n",
    "    '''Creates an infinite well potential across the x range.'''\n",
    "    return np.zeros(len(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def barrier_potential(x, left, right, height):\n",
    "    '''Creates a finite-sized barrier or well potential in the x range.'''\n",
    "    potential = np.zeros(len(x))\n",
    "    assert x[0] < left < right < x[-1], 'Improper barrier location specified.'\n",
    "    iLeft = x.searchsorted(left)\n",
    "    iRight = x.searchsorted(right)\n",
    "    potential[iLeft:iRight] = height\n",
    "    return potential"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sho_potential(x, frequency=1, center=0, mass=1):\n",
    "    '''Creates a simple harmonic oscillator potential on the x range.'''\n",
    "    return 0.5 * mass * (frequency**2) * (x - center)**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## User Guide\n",
    "To use the `Hamiltonian` class in the TDSE module, you need to give it three required arguments:\n",
    "1. The discretized $x$ dimension.\n",
    "2. The discretized $t$ dimension.\n",
    "3. A tuple to specify the potential function's values:\n",
    "  * The pure function that specifies the function shape.\n",
    "  * The arguments of that pure function.\n",
    "\n",
    "Optional keyword arguments are `boundary`, which can be `'hardwall'` or `'periodic'`, and `mass`, which factors into the kinetic term.\n",
    "\n",
    "To use the `TDSEViewer` class, you need to provide only $\\vec\\psi(t=0)$. Optional keyword arguments are \n",
    "* `x`, the discretized $x$ dimension. (Range taken as `(0, 1)` if not provided.)\n",
    "* `potential`, the potential function's values.\n",
    "* `normonly`, turns on or off plotting the Real and Imaginary components of $\\vec\\psi$.\n",
    "\n",
    "## TDSE Example\n",
    "First, discretize space and time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xgrid = np.linspace(0, 10, 500)\n",
    "tgrid = np.arange(0, 3, 0.001)\n",
    "frame = 50 # how frequently plots are regenerated"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, define your hamiltonian. This will be a simple hardwall system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hardwall = (hardwall_potential, None)\n",
    "hamiltonian = TDSE.Hamiltonian(xgrid, hardwall, tgrid, boundary='hardwall')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create the initial state of the wavefunction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wavefunction = gaussian_wavepacket(xgrid, mu=2, sigma=0.5, k=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create the viewer, and loop through time to solve the time-dependent Schr&ouml;dinger equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "viewer = TDSE.TDSEViewer(wavefunction, xgrid, hamiltonian.potential_values, normonly=False)\n",
    "for i, t in enumerate(tgrid):\n",
    "    viewer.wavefunction = hamiltonian.evolve(viewer.wavefunction)\n",
    "    if i % frame == 0:\n",
    "        viewer.update_lines()\n",
    "        viewer.set_title(round(t, 6))\n",
    "viewer.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Other examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# finite barrier\n",
    "xgrid = np.linspace(0, 10, 500)\n",
    "tgrid = np.arange(0, 3, 0.001)\n",
    "frame = 50\n",
    "\n",
    "barrier = (barrier_potential, (5, 6, 50))\n",
    "hamiltonian = TDSE.Hamiltonian(xgrid, barrier, tgrid, boundary='hardwall')\n",
    "\n",
    "wavefunction = gaussian_wavepacket(xgrid, mu=2, sigma=0.5, k=10)\n",
    "viewer = TDSE.TDSEViewer(wavefunction, xgrid, hamiltonian.potential_values)\n",
    "\n",
    "for i, t in enumerate(tgrid):\n",
    "    viewer.wavefunction = hamiltonian.evolve(viewer.wavefunction)\n",
    "    if i % frame == 0:\n",
    "        viewer.update_lines()\n",
    "        viewer.set_title(round(t, 6))\n",
    "viewer.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# finite well\n",
    "xgrid = np.linspace(0, 10, 500)\n",
    "tgrid = np.arange(0, 3, 0.001)\n",
    "frame = 50\n",
    "\n",
    "barrier = (barrier_potential, (3, 7, -100))\n",
    "hamiltonian = TDSE.Hamiltonian(xgrid, barrier, tgrid, boundary='hardwall')\n",
    "\n",
    "wavefunction = gaussian_wavepacket(xgrid, mu=2, sigma=0.5, k=10)\n",
    "viewer = TDSE.TDSEViewer(wavefunction, xgrid, hamiltonian.potential_values)\n",
    "\n",
    "for i, t in enumerate(tgrid):\n",
    "    viewer.wavefunction = hamiltonian.evolve(viewer.wavefunction)\n",
    "    if i % frame == 0:\n",
    "        viewer.update_lines()\n",
    "        viewer.set_title(round(t, 6))\n",
    "viewer.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# simple harmonic oscillator with evolution\n",
    "xgrid = np.linspace(0, 10, 500)\n",
    "tgrid = np.arange(0, 10, 0.001)\n",
    "frame = 50\n",
    "\n",
    "sho = (sho_potential, (2, 5, 1))\n",
    "hamiltonian = TDSE.Hamiltonian(xgrid, sho, tgrid, boundary='hardwall')\n",
    "\n",
    "wavefunction = gaussian_wavepacket(xgrid, mu=3, sigma=1, k=0)\n",
    "viewer = TDSE.TDSEViewer(wavefunction, xgrid, hamiltonian.potential_values)\n",
    "viewer.ylim(-1, 1)\n",
    "\n",
    "for i, t in enumerate(tgrid):\n",
    "    viewer.wavefunction = hamiltonian.evolve(viewer.wavefunction)\n",
    "    if i % frame == 0:\n",
    "        viewer.update_lines()\n",
    "        viewer.set_title(round(t, 6))\n",
    "viewer.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# simple harmonic oscillator with stationary state\n",
    "xgrid = np.linspace(0, 10, 500)\n",
    "tgrid = np.arange(0, 10, 0.001)\n",
    "frame = 50\n",
    "\n",
    "sho = (sho_potential, (1, 5, 1))\n",
    "hamiltonian = TDSE.Hamiltonian(xgrid, sho, tgrid, boundary='hardwall')\n",
    "\n",
    "wavefunction = gaussian_wavepacket(xgrid, mu=5, sigma=1, k=0)\n",
    "viewer = TDSE.TDSEViewer(wavefunction, xgrid, hamiltonian.potential_values)\n",
    "\n",
    "for i, t in enumerate(tgrid):\n",
    "    viewer.wavefunction = hamiltonian.evolve(viewer.wavefunction)\n",
    "    if i % frame == 0:\n",
    "        viewer.update_lines()\n",
    "        viewer.set_title(round(t, 6))\n",
    "viewer.close()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ATLAS",
   "language": "python",
   "name": "atlas"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
