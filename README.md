A repository of jupyter notebooks for solving simple quantum-mechanical and
statistical-mechanical systems.

1. `Notebook1`: A basic introduction to python for those who have never seen it
before.
1. `Notebook2`: An introduction to finite-difference calculus. All of the major
functionality relies on this.
1. `Notebook3`: Solving the Schr&ouml;dinger equation when discretized in space.
1. `Notebook4`: Solving the Schr&ouml;dinger equation when discretized in space
and time.
1. `Notebook5`: Finding eigenstates of Hamiltonians.
1. `Notebook6`: Using Hartree-Fock for multi-particle systems.