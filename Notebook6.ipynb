{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hartree-Fock\n",
    "Multi-electron atoms can be modeled using the Hartree Approximation. This is a somewhat intuitive model, where one accounts for electron-electron interactions via a self-consistent potential. That is, in order to model the ground state of an $N$-electron system, one fills the lowest $N$ eigenstates of the following Schr&ouml;dinger equation:\n",
    "$$\\left[-\\frac{\\nabla^2}{2m} + V_{\\rm eff}(r)\\right] \\phi_n(r)=\\epsilon_n \\phi_n(r).$$\n",
    "\n",
    "The effective potential is chosen so that $$V_{\\rm eff}(r) = V_{\\rm ext}(r) + \\int dr^\\prime\\, V_{ee}(r-r^\\prime) \\rho(r^\\prime),$$ where $V_{\\rm ext}(r)$ is the external potential (e.g. the Coulomb potential from the nucleus), $V_{ee}(r)$ is the potential energy of two electrons separated by distance $r$ (another Coulomb potential), and the electron distribution is $$\\rho(r) =\\sum_{n=1}^N |\\phi_n(r)|^2.$$ Because $V_{\\rm eff}$ depends on the solution of the Schr&ouml;dinger equation, we call it a \"self-consistent potential.\"\n",
    "\n",
    "A typical way to solve these equations is through iteration. One follows the following procedure:\n",
    "1. Makes an initial guess for $\\rho$. For example, $\\rho=0$.\n",
    "1. Solve the Schr&ouml;dinger equation to calculate the states $\\phi_n$.\n",
    "1. Calculate $\\rho$ from these $\\phi_n$'s.\n",
    "1. Recalculate $V_{\\rm eff}$ from this new $\\rho$.\n",
    "1. Repeat steps 2-4."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.sparse import linalg\n",
    "import TDSE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Aside:\n",
    "We will need a function which will take an array of eigenvectors, and will normalize them.  That is, if $\\delta$ is the spacing between lattice sites, we want them normalized so that in the end $$\\int|\\phi(x)|^2 dx\\approx\\sum_j |\\phi(x_j)|^2 \\delta =1.$$\n",
    "The following function performs this procedure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def normalizedEigenvectors(xgrid, eigenvectors):\n",
    "    '''Normalizes a list of eigenvectors according to the finite-grid xgrid.'''\n",
    "    xDelta = xgrid[1] - xgrid[0]\n",
    "    oldPsiSquared = abs(eigenvectors)**2\n",
    "    oldNorms = oldPsiSquared.sum(0)\n",
    "    return eigenvectors / np.sqrt(oldNorms * xDelta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 1: Fermi Gas in a Harmonic Trap\n",
    "Here, we will use the Hartree approximation to calculate the density profile of a gas of fermionic Lithium atoms in a 1D harmonic oscillator potential. That is, we imagine that we have a 1D harmonic trap, that we put $N$ atoms in. With $\\hbar = m = 1$, the external potential is $$ V_{\\rm ext} = x^2/2,$$ The interaction between neutral atoms is short-range, so the atom-atom interactions can be well-approximated as $$ V_{aa}=g \\delta(x), $$ where $g$ is a constant which parameterizes the interaction strength.\n",
    "\n",
    "Thus, we need to solve $$\\left[-\\frac{\\partial_x^2}{2} + V_{\\rm eff}(x)\\right] \\phi_n(x)=\\epsilon_n \\phi_n(x),$$ where $$V_{\\rm eff}(x) = V_{\\rm ext}(x) + \\int dx^\\prime\\, V_{a}(x-x^\\prime) \\rho(x^\\prime) = x^2/2 + g \\rho(x),$$ and $$\\rho(x)=\\sum_{n=1}^N |\\phi_n(x)|^2.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xgrid = np.linspace(-5, 5, 100)\n",
    "sho = 0.5 * xgrid**2\n",
    "hamiltonian = TDSE.Hamiltonian(xgrid, sho)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eVals, eVecs = linalg.eigsh(hamiltonian.hamiltonian, k=5, sigma=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "for eVec in eVecs.T:\n",
    "    plt.plot(xgrid, abs(eVec)**2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we want to calculate rho.  We can again use the \"sum\" method to do this.  The following code generates a bunch of eigenvectors, then sums them, plotting the individual eigenvectors and their sums"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "normEVecs = normalizedEigenvectors(xgrid, eVecs)\n",
    "psiSquared = abs(normEVecs)**2\n",
    "density = psiSquared.sum(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "plt.plot(xgrid, density)\n",
    "for normEVec in normEVecs.T:\n",
    "    plt.plot(xgrid, abs(normEVec)**2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example 2: Fermi gas in a square box."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xgrid = np.linspace(-5, 5, 200)\n",
    "\n",
    "hw = np.zeros(len(xgrid))\n",
    "hamiltonian = TDSE.Hamiltonian(xgrid, hw, boundary='hardwall')\n",
    "\n",
    "eVals, eVecs = linalg.eigsh(hamiltonian.hamiltonian, k=20, sigma=0)\n",
    "normEVecs = normalizedEigenvectors(xgrid, eVecs)\n",
    "\n",
    "psiSquared = abs(normEVecs)**2\n",
    "density = psiSquared.sum(1)\n",
    "\n",
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "plt.plot(xgrid, density)\n",
    "for eVec in normEVecs.T:\n",
    "    plt.plot(xgrid, abs(eVec)**2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The density is mostly flat, with some wiggles near the edge. These wiggles are known as \"Friedel Oscillations\" and can be important in solid state systems. If everything works, the area under the density curve should be exactly 20."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Automation\n",
    "Now we want to make a function which when called with a density, solves the Schrodinger equation, and spits out a new density. $$V_{\\rm eff}(x) = \\frac{1}{2}x^2 + g \\rho(x)$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def hartreeIterate(x, oldDensity, nAtoms, g=1):\n",
    "    '''Takes a density, generates Hartree potential, solves the S.E., and returns the new density.'''\n",
    "    # calculate the grid size\n",
    "    deltaX = x[1] - x[0]\n",
    "    # construct the potential\n",
    "    vExt = 0.5 * x**2\n",
    "    vEff = vExt + g * oldDensity\n",
    "    # construct the hamiltonian\n",
    "    hamiltonian = TDSE.Hamiltonian(x, vEff, boundary='hardwall')\n",
    "    # find the stationary states, and normalize\n",
    "    eVals, eVecs = linalg.eigsh(hamiltonian.hamiltonian, k=nAtoms, sigma=0)\n",
    "    normEVecs = normalizedEigenvectors(x, eVecs)\n",
    "    # calculate the density\n",
    "    psiSquared = abs(normEVecs)**2\n",
    "    density = psiSquared.sum(1)\n",
    "    return density"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test with the following.  This shows two Hartree iterations, starting with a \"guess\" of zero"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xgrid = np.linspace(-10, 10, 500)\n",
    "density0 = np.zeros(len(xgrid))\n",
    "\n",
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "plt.plot(xgrid, density0)\n",
    "\n",
    "density1 = hartreeIterate(xgrid, density0, 20, 2)\n",
    "plt.plot(xgrid, density1)\n",
    "\n",
    "density2 = hartreeIterate(xgrid, density1, 20, 2)\n",
    "plt.plot(xgrid, density2)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a loop which goes through 50 iterations,with 10 particles, and g=2 -- plotting the density at each step.  You should find that the algorithm converges, and the density profile is wider than the profile you find for non-interacting particles (which is just the profile from the 1st iteration)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xgrid = np.linspace(-10, 10, 500)\n",
    "density = np.zeros(len(xgrid))\n",
    "\n",
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "for i in range(50):\n",
    "    density = hartreeIterate(xgrid, density, 10, 2)\n",
    "    plt.plot(xgrid, density)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For stronger interactions, the naive iteration algorithm is unstable, but there is a slight variation which works: mix the new solution with the old one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xgrid = np.linspace(-10, 10, 200)\n",
    "density = np.zeros(len(xgrid))\n",
    "\n",
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "plt.plot(xgrid, density)\n",
    "\n",
    "for i in range(50):\n",
    "    newdensity = hartreeIterate(xgrid, density, 10, 30)\n",
    "    plt.plot(xgrid, newdensity)\n",
    "    density = 0.9 * density + 0.1 * newdensity\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now a function which will automate this calculation -- taking the parameters, and spitting out the density profile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solveHartree(x, nAtoms, g, nIterations):\n",
    "    '''Iterates the Hartree process.\n",
    "    \n",
    "    For nAtoms fermions trapped in a 1D simple-harmonic oscillator potential, with a point interaction\n",
    "    of strength g. It iterates the equations nIterations times starting with a density of zero.'''\n",
    "    density = np.zeros(len(xgrid)) # initial guess\n",
    "    for i in range(nIterations):\n",
    "        newdensity = hartreeIterate(xgrid, density, nAtoms, g)\n",
    "        density = 0.9 * density + 0.1 * newdensity\n",
    "    return newdensity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test with the following line"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xgrid = np.linspace(-10, 10, 200)\n",
    "density30 = solveHartree(xgrid, nAtoms=10, g=30, nIterations=50)\n",
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "plt.plot(xgrid, density30)\n",
    "plt.xlabel('x')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we takes a density profile and a grid, and return the RMS size of the cloud, $$\\sigma=\\sqrt{\\frac{\\int \\rho(x) x^2 dx}{\\int \\rho(x)  dx}}.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def width(xgrid, density):\n",
    "    '''Calculates the RMS width of a density profile on a grid.'''\n",
    "    integrand1 = density * xgrid**2\n",
    "    integrand2 = density\n",
    "    sum1 = integrand1.sum()\n",
    "    sum2 = integrand2.sum()\n",
    "    return np.sqrt(sum1 / sum2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is used in the following manner:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "width(xgrid, density30)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the case of 10 particles, we can calculate and plot the width $\\sigma$ for different values of $g$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gs = [0, 5, 10, 15, 20]\n",
    "sigmas = []\n",
    "\n",
    "xgrid = np.linspace(-10, 10, 200)\n",
    "for g in gs:\n",
    "    density = solveHartree(xgrid, nAtoms=10, g=g, nIterations=50)\n",
    "    sigmas.append(width(xgrid, density))\n",
    "\n",
    "plt.figure(figsize=(5, 5), dpi=100)\n",
    "plt.plot(gs, sigmas)\n",
    "plt.xlabel('g')\n",
    "plt.ylabel('$\\sigma$')\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ATLAS",
   "language": "python",
   "name": "atlas"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
