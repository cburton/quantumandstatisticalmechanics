import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

import tkinter

import scipy.sparse.linalg as linalg
import scipy.sparse as sparse

class TDSEViewer(FigureCanvasTkAgg):
    '''A class for viewing 1D wavefunctions and potentials.'''
    def __init__(self, wavefunction, x=None, potential=None, normonly=False):
        # process input parameters
        self.wavefunction = wavefunction
        self.potential = potential
        self.grid = x
        self.normonly = normonly

        # create the gui
        self.master = tkinter.Tk()
        self.master.geometry('600x400+0+0')

        # setup the figure
        if self.grid is None:
            self.nx = len(self.wavefunction)
            self.grid = np.linspace(0, 1, self.nx)
        else:
            self.nx = len(self.grid)
        self.fig = Figure(figsize=(5, 5), dpi=100)
        self.axs = self.fig.add_subplot()
        self.axs.set_ylabel(r'$\left| \psi \right|^2$', color='C2')
        self.axs.tick_params(axis='y', labelcolor='C2')
        if self.potential is not None:
            self.vAxs = self.axs.twinx()
            self.vAxs.set_ylabel('Potential', color='C3')
            self.vAxs.tick_params(axis='y', labelcolor='C3')
        # add wider borders to the figure
        self.fig.subplots_adjust(left=0.15, right=0.85)

        # initalize the parent class
        super(FigureCanvasTkAgg, self).__init__(self.fig, self.master)

        # draw the figure
        self.draw_lines()
        self.get_tk_widget().pack(side='top', fill='both', expand=1)

    def force_redraw(self):
        self.draw()
        self.master.update()

    def set_geometry(self, width, height, offset):
        '''Changes the shape and offset of the tkinter window'''
        self.master.geometry(f'{width}x{height}+{offset}+{offset}')
        self.force_redraw()

    def draw_lines(self):
        '''Draws the wavefunction and potential (if supplied) on twin axes.'''
        # draw the potential function if it is supplied
        if self.potential is not None:
            assert len(self.potential)==self.nx, \
                'Potential and wavefunction do not match'
            self.vLines, = self.vAxs.plot(self.grid, self.potential, color='C3')

        assert len(self.wavefunction)==self.nx, \
            'X-grid and wavefunction do not match'
        realPsi = np.real(self.wavefunction)
        imagPsi = np.imag(self.wavefunction)
        normPsi = realPsi**2 + imagPsi**2
        if not self.normonly:
            self.realLines, = self.axs.plot(self.grid, realPsi, color='C0')
            self.imagLines, = self.axs.plot(self.grid, imagPsi, color='C1')
        self.normLines, = self.axs.plot(self.grid, normPsi, color='C2')

        self.force_redraw()

    def update_lines(self):
        '''Redraws the wavefunction and potential with new data.'''
        realPsi = np.real(self.wavefunction)
        imagPsi = np.imag(self.wavefunction)
        normPsi = realPsi**2 + imagPsi**2

        if not self.normonly:
            self.realLines.set_ydata(realPsi)
            self.imagLines.set_ydata(imagPsi)
        self.normLines.set_ydata(normPsi)

        self.force_redraw()

    def ylim(self, ymin, ymax):
        '''Rescales the wavefunction y axis'''
        self.axs.set_ylim(ymin, ymax)

    def ylimV(self, ymin, ymax):
        '''Rescales the potential y axis'''
        self.vAxs.set_ylim(ymin, ymax)

    def rescale(self):
        '''Zooms the wavefunction y axis to be tight'''
        realY = self.realLines.get_data()
        if not self.normonly:
            imagY = self.imagLines.get_data()
            normY = self.normLines.get_data()
            ymin = min(np.min(realY), np.min(imagY), np.min(normY))
            ymax = max(np.max(realY), np.max(imagY), np.max(normY))
        else:
            ymin = np.min(normY)
            ymax = np.max(normY)
        self.ylim(ymin, ymax)
        self.force_redraw()

    def rescaleV(self):
        '''Zooms the potential y axis to be tight'''
        ymin, ymax = min(self.potential), max(self.potential)
        self.ylimV(ymin,ymax)
        self.force_redraw()

    def set_title(self,title):
        '''Sets the window title'''
        self.master.wm_title(title)

    def close(self):
        self.master.destroy()

class Hamiltonian:
    '''todo'''
    def __init__(self, x, potential, t=None, boundary='periodic', mass=1):
        self.xgrid = x # the finite-difference approximation of the one spatial dimension
        self.tgrid = t # the finite-difference approximation of the time dimension
        if self.tgrid is not None:
            self.dt = self.tgrid[1] - self.tgrid[0] # the time-evolution step size
        else:
            self.dt = None
        self.boundary = boundary # the boundary condition
        self.mass = mass # factors into the kinetic term

        # parse the potential arguments
        if isinstance(potential, tuple):
            self.potential_function = potential[0] # pure function used to calculate the potential
            self.potential_args = potential[1] # arguments passed to potential_function
            if self.potential_args is None:
                self.potential_args = ()
            self.potential_values = None
        else:
            self.potential_function = None
            self.potential_args = None
            self.potential_values = potential

        # length of the x grid
        self.nx = len(self.xgrid)
        # just an identity matrix of the correct size (to avoid re-building in loops)
        self.identity = sparse.identity(self.nx, dtype=complex, format='csc')

        # build the kinetic term just once
        ddx2 = second_derivative_matrix(self.xgrid, self.boundary)
        self.kinetic = -0.5 / self.mass * ddx2 # in hbar=1 units

        self.buildOperators()

    def buildOperators(self, potential_args=None):
        '''Recalculates the potential and full hamiltonian if the potential function.'''
        if potential_args is not None:
            self.potential_args = potential_args
        if self.potential_function is not None:
            self.potential_values = self.potential_function(self.xgrid, *self.potential_args)
        elif self.potential_values is not None:
            pass
        else:
            assert False, 'Error forming hamiltonian from the given potential.'
        self.potential = potential_matrix(self.potential_values)
        self.hamiltonian = sparse.csc_matrix(self.kinetic + self.potential)

        if self.dt is not None:
            leftInverse = self.identity + 0.5j * self.dt * self.hamiltonian
            self.left = linalg.factorized(leftInverse)
            self.right = self.identity - 0.5j * self.dt * self.hamiltonian

    def evolve(self, wavefunction):
        return self.left(self.right.dot(wavefunction))

def potential_matrix(potential):
    '''Generates sparse matrix for the given potential.'''
    nx = len(potential)
    return sparse.dia_matrix((potential, 0), shape=(nx, nx))

def second_derivative_matrix(x, boundary):
    nx = len(x)
    unitlist = np.ones(nx)

    dx = x[1] - x[0]
    dx2 = dx**2

    offdiag = unitlist / dx2
    diag = -2 * unitlist / dx2
    shape = (nx, nx)

    if boundary=='periodic':
        values = (offdiag, offdiag, diag, offdiag, offdiag)
        locations = (1, -1, 0, nx - 1, -nx + 1)
        return sparse.dia_matrix((values, locations), shape=shape)
    elif boundary=='hardwall':
        values = (offdiag, offdiag, diag)
        locations = (1, -1, 0)
        return sparse.dia_matrix((values, locations), shape=shape)
    else:
        assert False, f'Unknown boundary condition: "{boundary}".'