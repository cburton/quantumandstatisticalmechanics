{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Solving the Time-dependent Schroedinger Equation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model\n",
    "\n",
    "Consider a two-state system with $\\psi_L$ is the amplitude for particle on the left, and $\\psi_R$ is the amplitude for it to be on the right. By L/R symmetry, we can say that $$ i\\partial_t \\left(\\begin{array}{c}\\psi_L(t)\\\\\\psi_R(t)\\end{array}\\right)=\n",
    "\\left(\\begin{array}{cc}a&b\\\\ b&a\\end{array}\\right)\\left(\\begin{array}{c}\\psi_L(t)\\\\\\psi_R(t)\\end{array}\\right).$$\n",
    "\n",
    "Use a gauge change to get rid of `a` by taking $\\psi\\to e^{-i a t} \\psi$.  Then, choose a unit of time so that $b=-1$.  Thus, we will try to solve\n",
    "\n",
    "$$ i\\partial_t \\left(\\begin{array}{c}\\psi_L(t)\\\\\\psi_R(t)\\end{array}\\right)=\n",
    "\\left(\\begin{array}{cc}0&-1\\\\ -1&0\\end{array}\\right)\\left(\\begin{array}{c}\\psi_L(t)\\\\\\psi_R(t)\\end{array}\\right).$$\n",
    "\n",
    "This is a simple enough equation to solve analytically, but we want to try using numerical tools. We will discretize time and use a finite difference approximation (of both space and time).\n",
    "\n",
    "Some definitions: $$\\vec\\psi(t)=\\left(\\begin{array}{c}\\psi_L(t)\\\\\\psi_R(t)\\end{array}\\right), \\text{ and } H=\\left(\\begin{array}{cc}0&-1\\\\ -1&0\\end{array}\\right)$$\n",
    "\n",
    "The following are two finite-difference schemes.\n",
    "$$ \\mbox{ Euler:}\\quad i\\frac{\\vec\\psi(t+\\delta t)-\\vec \\psi(t)}{\\delta t}= H\\vec\\psi(t)$$\n",
    "\n",
    "$$\\mbox{ Backwards Euler:}\\quad i\\frac{\\vec\\psi(t+\\delta t)-\\vec \\psi(t)}{\\delta t}= H\\vec\\psi(t+\\delta t)$$\n",
    "\n",
    "Unfortunately, these do not conserve probability, so we'll use a modified form: the Unitary Euler method:\n",
    "\n",
    "$$\\mbox{ Unitary Euler:}\\quad i\\frac{\\vec\\psi(t+\\delta t)-\\vec \\psi(t)}{\\delta t}= H\\left(\\frac{\\vec\\psi(t+\\delta t)+\\vec\\psi(t)}{2}\\right).$$\n",
    "\n",
    "Rearranging this to separate $\\vec\\psi(t+\\delta t)$ from $\\vec\\psi(t)$, we have\n",
    "$$\\left(1+\\frac{i H \\delta t}{2}\\right)\\vec\\psi(t+\\delta t)= \\left(1-\\frac{i H \\delta t}{2}\\right)\\vec\\psi(t).$$\n",
    "\n",
    "We can then invert the left-hand-side matrix $(1+i H \\delta t/2)$ in order to write the equation in the form $\\vec\\psi(t+\\delta t)=U\\vec\\psi(t)$ where $U$ is a $2\\times 2$ matrix. Doing that, $U$ is the product of two matrices,\n",
    "$U=\\left(1+i H \\delta t/2\\right)^{-1} \\left(1-i H \\delta t/2\\right).$\n",
    "\n",
    "In matrix form, we have\n",
    "$$\n",
    "(1+i H \\delta t/2)=\\left(\\begin{array}{cc} 1 & -i \\delta t \\\\  -i \\delta t & 1 \\end{array}\\right)\n",
    "\\text{ and } \n",
    "(1-i H \\delta t/2)=\\left(\\begin{array}{cc} 1 & i \\delta t \\\\ i \\delta t & 1 \\end{array}\\right)\n",
    "$$\n",
    "\n",
    "## Matrices\n",
    "Recall that one can build matrices in Python with the following notation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example_matrix = np.array([[1, 2],[3, 4]])\n",
    "print(example_matrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can invert matrices using the following notation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example_inverse = np.linalg.inv(example_matrix)\n",
    "print(example_inverse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify that this is indeed the inverse by taking the matrix product:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example_inverse.dot(example_matrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Complex Numbers\n",
    "In Python, you can define complex numbers using a lower case $j$, such as"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(3 + 2j) * (1 - 1j)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finite-difference Schroedinger Equation\n",
    "Build the following function by replacing all of the letter placeholders."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def U(dt):\n",
    "    \"\"\"U(dt) generates a 2x2 matrix which evolves the wavefunction for Ammonia by a time dt. \n",
    "    We use units where the level spacing is unity.\"\"\"\n",
    "    mat1=array([[a,b],[c,d]]) # this should be 1+i H dt/2\n",
    "    mat2=array([[e,f],[g,h]]) # this should be 1-i H dt/2\n",
    "    U=inv(mat1).dot(mat2)\n",
    "    return U"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Chuck's solution\n",
    "def U(dt):\n",
    "    mat1 = np.array([[1, -dt * 1j], [-dt * 1j, 1]])\n",
    "    mat2 = np.array([[1, dt * 1j], [dt * 1j, 1]])\n",
    "    U = np.linalg.inv(mat1).dot(mat2)\n",
    "    return U"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test it with the following code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "U1 = U(0.1)\n",
    "U1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Time Evolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To calculate the time evolution, we simply need to repeatedly multiply by this matrix.  For that we need \"loops.\"  Here is a little \"program\" which calculates a time-sequence of wavefunctions.  [One could encapsulate this into a function, but I find many students are more comfortable simply copying and pasting code blocks like this one.]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt = 0.1 # set timestep\n",
    "evolve = U(dt) # generate matrix which evolves in time\n",
    "psi0 = np.array([1, 0]) # initial condition\n",
    "maxt = 10 # what number to integrate to\n",
    "\n",
    "t = 0\n",
    "psi = psi0\n",
    "tlist = [t]\n",
    "psilist = [psi]\n",
    "\n",
    "while (t < maxt):\n",
    "    t = t + dt\n",
    "    psi = evolve.dot(psi)\n",
    "    tlist.append(t)\n",
    "    psilist.append(psi)\n",
    "\n",
    "psiarray = np.array(psilist)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From there, we can calculate the probability function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "probabilityarray = abs(psiarray)**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first column corresponds to the probability of being on the left, while the second column corresponds to being on the right."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "left_probability = probabilityarray[:, 0]\n",
    "right_probability = probabilityarray[:, 1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(tlist, left_probability, label='$|\\psi_L|^2$')\n",
    "plt.plot(tlist, right_probability, label='$|\\psi_R|^2$')\n",
    "plt.ylabel('Probability')\n",
    "plt.xlabel('$t$')\n",
    "plt.ylim(0, 1.25)\n",
    "plt.legend(frameon=False, ncol=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An important question is how small to take our step-size.  Modify the following code to produce a stepsize of dt=0.01.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt = 0.01 # set timestep\n",
    "evolve = U(dt) # generate matrix which evolves in time\n",
    "psi0 = np.array([1, 0]) # initial condition\n",
    "maxt = 10 # what number to integrate to\n",
    "\n",
    "t = 0\n",
    "psi = psi0\n",
    "tlist2 = [t]\n",
    "psilist2 = [psi]\n",
    "\n",
    "while (t < maxt):\n",
    "    t = t + dt\n",
    "    psi = evolve.dot(psi)\n",
    "    tlist2.append(t)\n",
    "    psilist2.append(psi)\n",
    "\n",
    "psiarray2 = np.array(psilist2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code makes a plot which compares the results for dt=0.1 and dt=0.01"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(tlist2, abs(psiarray2[:,0])**2, '.', label='$\\delta t = 0.1$')\n",
    "plt.plot(tlist, abs(psiarray[:,0])**2, 'o', label='$\\delta t = 0.01$')\n",
    "plt.ylim(0, 1.25)\n",
    "plt.ylabel('$|\\psi_L|^2$')\n",
    "plt.xlabel('$t$')\n",
    "plt.legend(frameon=False, ncol=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cells below, repeat the dt=0.1 calculation, but with the initial state being $|+\\rangle=(1/\\sqrt{2},1/\\sqrt{2})$.  For obvious reasons, we call $|+\\rangle$ a \"stationary state\".  <em> Note, your plot will look funny unless you include the command <tt>ylim(0,1)</tt> which sets the y-axis range.  The plotting functions sometimes have difficulty with automatically choosing the range </em>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Chuck's solution\n",
    "dt = 0.01 # set timestep\n",
    "evolve = U(dt) # generate matrix which evolves in time\n",
    "psi0 = np.array([1/np.sqrt(2), 1/np.sqrt(2)]) # initial condition\n",
    "maxt = 10 # what number to integrate to\n",
    "\n",
    "t = 0\n",
    "psi = psi0\n",
    "tlist3 = [t]\n",
    "psilist3 = [psi]\n",
    "\n",
    "while (t < maxt):\n",
    "    t = t + dt\n",
    "    psi = evolve.dot(psi)\n",
    "    tlist3.append(t)\n",
    "    psilist3.append(psi)\n",
    "\n",
    "psiarray3 = np.array(psilist3)\n",
    "\n",
    "plt.plot(tlist3, abs(psiarray3[:,0])**2, '.', label='$\\delta t = 0.01$')\n",
    "plt.ylim(0, 1)\n",
    "plt.ylabel('$|\\psi_L|^2$')\n",
    "plt.xlabel('$t$')\n",
    "plt.legend(frameon=False, ncol=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stimulated Absorption and Emission\n",
    "\n",
    "In the presence of an electromagnetic field, the equations of motion become\n",
    "\n",
    "$$ i\\partial_t \\left(\\begin{array}{c}\\psi_L(t)\\\\\\psi_R(t)\\end{array}\\right)=\n",
    "\\left(\\begin{array}{cc}E(t)&-1\\\\ -1&-E(t)\\end{array}\\right)\\left(\\begin{array}{c}\\psi_L(t)\\\\\\psi_R(t)\\end{array}\\right),$$\n",
    "\n",
    "where $E(t)=\\epsilon \\cos(\\omega t)$. Now that $H$ is time-dependent, the generalization of the Unitary Euler method is\n",
    "\n",
    "$$i\\frac{\\vec\\psi(t+\\delta t)-\\vec \\psi(t)}{\\delta t}= H(t+\\delta t/2)\\left(\\frac{\\vec\\psi(t+\\delta t)+\\vec\\psi(t)}{2}\\right)$$\n",
    "\n",
    "This can be rewritten as\n",
    "\n",
    "$$\\psi(t+\\delta t)= U(t) \\psi(t), $$\n",
    "where\n",
    "$$\n",
    "U(t)=\\left(\\begin{array}{cc}\n",
    "2i - \\delta t E &  dt \\\\  dt & 2i - \\delta t E \n",
    "\\end{array}\\right)^{-1} \n",
    "\\left(\\begin{array}{cc} \n",
    "2i + \\delta t E & -dt \\\\ -dt & 2i + \\delta t E\n",
    "\\end{array}\\right)\n",
    "$$\n",
    "\n",
    "Now, make a Python function which generates this $U$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def drivenU(dt, E):\n",
    "    '''U(dt,E) generates a 2x2 matrix which evolves the wavefunction for Ammonia by a time dt\n",
    "    in the presence of an oscillating electric field.  The quantity E, should be the electric field\n",
    "    at time t+dt/2.\n",
    "    We use units where the level spacing is unity.'''\n",
    "    mat1 = array([[a, b],[c, d]]) # this should be 1+i H dt/2\n",
    "    mat2 = array([[e, f],[g, h]]) # this should be 1-i H dt/2\n",
    "    U = np.linalg.inv(mat1).dot(mat2)\n",
    "    return U"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Chuck's solution\n",
    "def drivenU(dt, E):\n",
    "    mat1 = np.array([[2 * 1j - dt * E, dt],[dt, 2 * 1j - dt * E]])\n",
    "    mat2 = np.array([[2 * 1j, -dt],[-dt, 2 * 1j + dt * E]])\n",
    "    U = np.linalg.inv(mat1).dot(mat2)\n",
    "    return U"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The particle starts in the initial state $|+\\rangle=(1/\\sqrt{2},1/\\sqrt{2})$. Take $E=\\epsilon \\cos(\\omega t)$ with $\\epsilon=0.1$ and $\\omega=1$. This is off-resonant, and you should find very little stimulated absorption (at best a few percent).  You will have to generate $U$ inside your loop, as it is different at each time slice. Use $dt=0.1$, and integrate to time $t=100$. Plot the probability of being in the ground state $P_g=| \\psi_L/\\sqrt{2}+\\psi_R/\\sqrt{2}|^2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt = 0.01\n",
    "psi0 = np.array([1/np.sqrt(2), 1/np.sqrt(2)])\n",
    "maxt = 100\n",
    "epsilon = 0.1\n",
    "frequency = 1\n",
    "\n",
    "t = 0\n",
    "psi = psi0\n",
    "\n",
    "tlist = [t]\n",
    "psilist = [psi]\n",
    "\n",
    "while t < maxt:\n",
    "    t = t + dt\n",
    "    evolve = drivenU(dt, epsilon * np.cos(frequency * (t + dt / 2)))\n",
    "    psi = evolve.dot(psi)\n",
    "    tlist.append(t)\n",
    "    psilist.append(psi)\n",
    "    \n",
    "psiarray = np.array(psilist)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "psiL = psiarray[:, 0]\n",
    "psiR = psiarray[:, 1]\n",
    "ground_state = abs(psiL/np.sqrt(2) + psiR/np.sqrt(2))**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(tlist, ground_state)\n",
    "plt.ylim(0, 1)\n",
    "plt.xlabel('$t$')\n",
    "plt.ylabel('$\\psi_{gs}(t)$')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat with a resonant drive, where $\\omega=2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt = 0.01\n",
    "psi0 = np.array([1/np.sqrt(2), 1/np.sqrt(2)])\n",
    "maxt = 100\n",
    "epsilon = 0.1\n",
    "frequency = 2\n",
    "\n",
    "t = 0\n",
    "psi = psi0\n",
    "\n",
    "tlist = [t]\n",
    "psilist = [psi]\n",
    "\n",
    "while t < maxt:\n",
    "    t = t + dt\n",
    "    evolve = drivenU(dt, epsilon * np.cos(frequency * (t + dt / 2)))\n",
    "    psi = evolve.dot(psi)\n",
    "    tlist.append(t)\n",
    "    psilist.append(psi)\n",
    "    \n",
    "psiarray = np.array(psilist)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "psiL = psiarray[:, 0]\n",
    "psiR = psiarray[:, 1]\n",
    "ground_state = abs(psiL/np.sqrt(2) + psiR/np.sqrt(2))**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(tlist, ground_state)\n",
    "plt.ylim(0, 1)\n",
    "plt.xlabel('$t$')\n",
    "plt.ylabel('$\\psi_{gs}(t)$')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
